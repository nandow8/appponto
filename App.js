import React from 'react';
import {AppLoading} from 'expo';
import * as Icons from '@expo/vector-icons';
import {Asset} from 'expo-asset';
import * as Font from 'expo-font'
import { NavigationContainer } from '@react-navigation/native';
import AppNavigation from './src/routes/Navigation';

export default class App extends React.Component {

  state = {
      isLoadingComplete: false,
      outroLoadingComplete: false,
  };

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Icons.font,
    });
    this.setState({outroLoadingComplete: true})
  }

  render() {
      if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
          return (
              <AppLoading
                  startAsync={this._loadResourcesAsync}
                  onError={this._handleLoadingError}
                  onFinish={this._handleFinishLoading}
              />
          );
      } else {
          return (
            <NavigationContainer>
              <AppNavigation/>
              </NavigationContainer>
          );
      }
  }

  _loadResourcesAsync = async () => {
      return Promise.all([
          Asset.loadAsync([
              require('./assets/splash.png'),
              require('./assets/splash.png'),
              require('./assets/splash.png'),
          ]),
          Font.loadAsync({
              // This is the font that we are using for our tab bar
              ...Icons.Ionicons.font,
          }),
      ]);
  };

  _handleLoadingError = error => {
      // In this case, you might want to report the error to your error
      // reporting service, for example Sentry
      console.warn(error);
  };

  _handleFinishLoading = () => {
      this.setState({ isLoadingComplete: true });
  };

}