const React = require('react-native')
import { StyleSheet } from "react-native";

export default {
      container: {
        // flex: 1, 993622
        alignItems: 'center',
        backgroundColor: 'gray'
      },
      overlay: {
        opacity: 0.5,
        
      },
      logo: {
        width: 160,
        height: 100,
        marginBottom: 10
      },
      headline: {
        fontSize: 18,
        textAlign: 'center',
        
        color: 'white'
      },

      textButton: {
        textAlign: 'center',
        color: '#000'
      },

      buttonHeader: {
          backgroundColor: 'lightgray',
          paddingBottom: 1
      },

      buttonRegistrarPonto: {
        marginLeft: '25%',
        marginRight: '25%',
        marginTop: 65
      },

      viewLocalizacaoIdentificada: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 40,
      },

      viewAtualizar: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
      },

      atualizar: {
        color: 'blue',
        marginTop: 10,
        fontSize: 13
      },

      iconLocalizacaoIdentificada: {
        fontSize: 20,
        color: 'darkgreen'
      },
      localizacaoBuscaIdentificada: {
        textAlign: 'center',
        fontSize: 14,
        color: 'blue'
      },
      localizacaoIdentificada: {
        textAlign: 'center',
        fontSize: 14,
        color: 'green'
      },
      localizacaoNaoIdentificada: {
        textAlign: 'center',
        fontSize: 14,
        color: 'red'
      }
} 