import React, { useState, useEffect, useRef } from 'react';
import { Text, Alert, View, StyleSheet, SafeAreaView, TouchableOpacity, Modal, ActivityIndicator, Image, AsyncStorage } from 'react-native';
import {Button} from 'native-base'
import { Col, Row, Grid } from "react-native-easy-grid";
import { Camera } from 'expo-camera';
import {FontAwesome} from '@expo/vector-icons'
import UrlHelper from '../../constants/UrlHelper';
import axios from 'axios';

export default function CameraBaterPonto(props) {
    const camRef = useRef(null)
    const [type, setType] = useState(Camera.Constants.Type.back);
    const [hasPermission, setHasPermission] = useState(null)
    const [capturedPhoto, setCapturedPhoto] = useState(null)
    const [foto, setFoto] = useState(null)
    const [isLoading, setisLoading] = useState(false)
    const [open, setOpen] = useState(false)
    
    useEffect(() => {
        (async () => {
            const {status} = await Camera.requestPermissionsAsync()
            setHasPermission(status === 'granted')
        })();
    }, [])

    if (hasPermission === null) {
        return <View/>
    }
    
    if (hasPermission === false) {
        return <Text>Acesso negado!</Text>
    }

    async function takePicture() {
        setisLoading(true)
        if (camRef) {
            const options = { quality: 0.5, base64: true, forceUpOrientation: true, fixOrientation: true, };
            const data = await camRef.current.takePictureAsync(options)
            setFoto(data.base64);
            setCapturedPhoto(data.uri)
            setOpen(true)
        }
        setisLoading(false)
    }
    
    async function registrarPonto() {
        setisLoading(true)
        let user = JSON.parse(await AsyncStorage.getItem('user'))
        
        await axios.post(UrlHelper.apiUrl(`registro_marcacao_ponto`),
            {
                "users_id": user.id,
                "empresas_id": user.empresas_id,
                "foto": foto,
                "observacao_colaborador": 'opa',
            }
        ).then(res => {
            Alert.alert(
                "Sucesso",
                `Seu ponto foi registrado, bom trabalho. Comprovante: ${res.data.numero_recibo} `,
                [
                  { text: "OK", onPress: () => {
                    setOpen(false)
                    props.navigation.navigate('HomeScreen')
                  }}
                ],
                { cancelable: false }
            );
            setisLoading(false)
        }).catch(err => {
            Alert.alert(
                "Atenção",
                "Seu ponto não foi registrado, tente novamente.",
                [
                  { text: "OK", onPress: () => {
                    setOpen(false)
                    props.navigation.navigate('HomeScreen')
                  }}
                ],
                { cancelable: false }
            );
            setisLoading(false)
        })
        
    }

    return ( 
        <SafeAreaView style={styles.container}>
            <Camera 
                style={{ flex: 1}}
                type={type}
                ref={camRef}
            >
                <View style={{flex: 1, backgroundColor: 'transparent', flexDirection: 'row'}}>
                    <TouchableOpacity 
                        style={{ 
                            position: 'absolute', 
                            bottom: 20, 
                            left: 20
                        }}
                        onPress={() => {
                            setType(
                                type === Camera.Constants.Type.back
                                ? Camera.Constants.Type.front
                                : Camera.Constants.Type.back
                            )
                        }}
                    >
                        <Text style={{ fontSize: 20, marginBottom: 13, color: 'white'}}>Trocar câmera</Text>
                        <Text onPress={ () => {
                            setOpen(false)
                            //props.navigation.navigate('HomeScreen')
                        }} 
                        style={{ fontSize: 20, marginBottom: 13, color: 'white'}}>
                            Sair
                        </Text>
                    </TouchableOpacity>
                </View>
            </Camera>

            <TouchableOpacity style={styles.buttonCamera} onPress={ takePicture }>
                {
                    isLoading == true
                    ? <ActivityIndicator animating={isLoading} />
                    : <FontAwesome name="camera" size={23} color="#FFF" />
                }
            </TouchableOpacity>

            { capturedPhoto && 
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={open}
                >
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 20}}>
                        <Image
                            style={{width: '100%', marginTop: 20, height: '80%', borderRadius: 20}}
                            source={{ uri: capturedPhoto }}
                        />

                        <Grid>
                            <Col></Col>
                            <Col style={{ margin: 10}}>
                                <TouchableOpacity style={{ margin: 10}} onPress={() => setOpen(false)}>
                                    <FontAwesome name="close" size={50} color="#FF0000" />
                                </TouchableOpacity>
                            </Col>

                            <Col style={{ margin: 10}}>
                                <TouchableOpacity style={{ margin: 10}} onPress={ registrarPonto }>
                                    {
                                        isLoading == true
                                        ? <ActivityIndicator animating={isLoading} />
                                        : <FontAwesome name="check" size={50} color="green" />
                                    }
                                </TouchableOpacity>
                            </Col>
                            <Col></Col>
                        </Grid>
                    </View>
                </Modal>
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    buttonCamera: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#121212',
        margin: 20,
        borderRadius: 10,
        height: 50
    }
})