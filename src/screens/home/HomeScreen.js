import React, { Component } from 'react';
import styles from './Style'
import {  View, Alert, TouchableOpacity, AsyncStorage, ActivityIndicator } from 'react-native';
import { Button, Footer, FooterTab, Container, Content, Badge, Accordion, Icon, Text } from 'native-base';
import * as Location from 'expo-location'
import * as Permissions from 'expo-permissions'
import SubHeaderComponent from '../../components/SubHeaderComponent';
import HeaderComponent from '../../components/HeaderComponent';
import axios from 'axios'
import UrlHelper from '../../constants/UrlHelper'
import * as ImagePicker from 'expo-image-picker';

export default class HomeScreen extends Component {
  state = {
    pickerResult: null,
    cameraStatus: '',
    cameraRollStatus: '',

    location: {}, // { JSON.stringify(this.state.location) }
    errorBuscarLocalizacao: 0,
    messageLocalizacao: '',
    user: [],
    activityIndicatorLoading: false
  }

  UNSAFE_componentWillMount() {
    this._getLocation()
  }

  _getLocation = async () => {
    this.setState({ 
      errorBuscarLocalizacao: 0
    })

    const { status } = await Permissions.askAsync(Permissions.LOCATION)

    if (status !== 'granted') {
      console.log('NOT GRANTED')

      this.setState({
        errorBuscarLocalizacao: 2,
      })
    } else {
      this.setState({
        errorBuscarLocalizacao: 1,
      })
    }

    const location = await Location.getCurrentPositionAsync()

    this.setState({
      location
    })
  }

  verificaEscala_e_Coordenada = async () => {
    this.setState({
      activityIndicatorLoading: true
    })

    if (this.state.errorBuscarLocalizacao != 1) { // verifica se gps ok
      Alert.alert('Atenção', 'Não foi possível registrar o ponto, por favor, atualize a sua localização.')
      this.setState({
        activityIndicatorLoading: false
      })
    }

    let user = await JSON.parse(await AsyncStorage.getItem('user'))

    axios.post(UrlHelper.apiUrl(`verificasepodebaterponto`), {users_id: user.id, empresas_id: user.empresas_id}).then(res => {
      axios.post(UrlHelper.apiUrl(`verificadistanciabaterponto`), {users_id: user.id, lat: this.state.location.coords.latitude, lng: this.state.location.coords.longitude}).then(resLatLng => {

        this.setState({
          activityIndicatorLoading: false
        })

        if (res.data.permite_marcacao_ponto_app == 1) {
          this.takePicture()
        } else {
          
          axios.post(UrlHelper.apiUrl(`registro_marcacao_ponto`),
              {
                  "users_id": user.id,
                  "empresas_id": user.empresas_id,
                  "foto": null,
                  "observacao_colaborador": 'opa',
              }
            ).then(res => {
                Alert.alert(
                    "Sucesso",
                    `Seu ponto foi registrado, bom trabalho. Comprovante: ${res.data.numero_recibo} `
                );

                this.setState({
                  activityIndicatorLoading: false
                })
            }).catch(err => {
              this.setState({
                activityIndicatorLoading: false
              })
                Alert.alert(
                    "Atenção",
                    "Seu ponto não foi registrado, tente novamente.",
                );
            })
        }
        
        
      }).catch(err => {
        
        Alert.alert('Atenção', 'Não foi possível registrar o ponto, erro em sua localizacao.')
        this.setState({
          activityIndicatorLoading: false
        })
      }) 
      
    }).catch(err => {
      Alert.alert('Atenção', 'Não foi possível registrar o ponto, por favor, verifique a sua escala.')
      this.setState({
        activityIndicatorLoading: false
      })
    }) 
  }

  deslogar = async () => {
    AsyncStorage.removeItem('token');
    AsyncStorage.removeItem('user');
    
    this.props.navigation.navigate('LoginScreen');
  }

  takePicture = async () => {
    const permissions = Permissions.CAMERA;
    const permissions2 = Permissions.CAMERA_ROLL
    const status = await Permissions.askAsync(permissions);
    const status2 = await Permissions.askAsync(permissions2);
    let user = await JSON.parse(await AsyncStorage.getItem('user'))
    this.setState({
      activityIndicatorLoading: true,
      cameraStatus: status.status,
      cameraRollStatus: status2.status
    });

    if (status.status !== 'granted' && status2.status !=='granted') {
      this.setState({
        activityIndicatorLoading: false
      });
        // console.log(`[ pickFromCamera ] ${permissions} access: ${status.status}`);
        // console.log(`[ pickFromCamera ] ${permissions2} access: ${status2.status}`);
    
    } else {
        const pickerResult = await ImagePicker.launchCameraAsync({
            mediaTypes: 'Images',
            base64:true,
            aspect: [4,3],
            quality: 0.2,
            allowsEditing: false // permite edicao da foto
        });    

        if(!pickerResult.cancelled){
            this.setState({
                pickerResult,
            })
            
            await axios.post(UrlHelper.apiUrl(`registro_marcacao_ponto`),
              {
                  "users_id": user.id,
                  "empresas_id": user.empresas_id,
                  "foto": pickerResult.base64,
                  "observacao_colaborador": 'opa',
              }
            ).then(res => {
                Alert.alert(
                    "Sucesso",
                    `Seu ponto foi registrado, bom trabalho. Comprovante: ${res.data.numero_recibo} `
                );

                this.setState({
                  activityIndicatorLoading: false
                })
            }).catch(err => {
              this.setState({
                activityIndicatorLoading: false
              })
                Alert.alert(
                    "Atenção",
                    "Seu ponto não foi registrado, tente novamente.",
                );
            })
            //console.log(this.state.imageData)
        } else {
          this.setState({
            activityIndicatorLoading: false
          });
        }
    }
  }



  render() {
    return (
      <Container>
        <HeaderComponent />

        <Content >
          <SubHeaderComponent titulo="REGISTRO DE PONTO" icone="pencil"/>
          
          <View full style={styles.viewLocalizacaoIdentificada}>
            <Icon type="Entypo" name='location-pin' style={styles.iconLocalizacaoIdentificada} />
            
            {
              this.state.errorBuscarLocalizacao == 0 && (
                <Text style={styles.localizacaoBuscaIdentificada}>Buscando localização...</Text>
              )
            }
            {
              this.state.errorBuscarLocalizacao == 1 && (
                <Text style={styles.localizacaoIdentificada}>Localização identificada</Text>
              )
            }
            {
              this.state.errorBuscarLocalizacao == 2 && (
                <Text style={styles.localizacaoNaoIdentificada}>Localização não identificada, verifique sua conexão</Text>
              )
            }
          </View>

          <TouchableOpacity full style={styles.viewAtualizar} onPress={() => this._getLocation()}>
            <Text style={styles.atualizar}>ATUALIZAR</Text>
          </TouchableOpacity>

          {
            this.state.activityIndicatorLoading === true
            ? <ActivityIndicator size={140} color="blue" animating={this.state.activityIndicatorLoading} />
            : <Button 
                iconLeft 
                rounded 
                success 
                disabled={this.state.errorBuscarLocalizacao !== 1 ? true : false}
                style={styles.buttonRegistrarPonto}
                onPress={() => this.verificaEscala_e_Coordenada()}
                >
                  <Icon type="AntDesign" name='rightcircleo' />
                <Text>Registrar Ponto </Text>
              </Button>     
          }
        </Content>

        <Footer>
            <FooterTab>
                <Button vertical onPress={() => this.deslogar()}>
                  {/* <Badge ><Text>51</Text></Badge> */}
                  <Icon type="MaterialCommunityIcons" name="exit-run" style={{ fontSize: 25, color: 'orangered' }}/>
                  <Text>Deslogar</Text>
                </Button>

                <Button   vertical onPress={ () => this.props.navigation.navigate('HistoricodepontosScreen', 1)}>
                  {/* <Badge ><Text>51</Text></Badge> */}
                  <Icon style={{color: '#000'}} active name="paper" />
                  <Text style={{color: '#000'}}>Histórico de Pontos</Text>
                </Button>
            </FooterTab>
        </Footer>

      </Container>
    );
  }
}
