import React, { Component, useState } from 'react';
import { Modal, Text, TouchableOpacity, View, Alert,StyleSheet } from 'react-native';
import { Icon } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";

export default function ModalComponent() {
  const [modalVisible, setModalVisible] = useState(false);

  const modalHeader=(
    <View style={styles.modalHeader}>
      <Text style={styles.title}>Descrição das cores</Text>
      <View style={styles.divider}></View>
    </View>
  )

  const modalBody=(
    <View style={styles.modalBody}>
      <Grid>
        <Row>
          <Col>
            <Icon style={styles.permissaoAzul} type="SimpleLineIcons" name="arrow-right-circle" />
          </Col>
          <Col>
            <Text style={{ marginTop: 5 }}>OK</Text>
          </Col>
        </Row>

        <Row>
          <Col>
            <Icon style={styles.permissaoVerde} type="SimpleLineIcons" name="arrow-right-circle" />
          </Col>
          <Col>
            <Text style={{ marginTop: 5 }}>Respondido</Text>
          </Col>
        </Row>

        <Row>
          <Col>
            <Icon style={styles.permissaoLaranja} type="SimpleLineIcons" name="arrow-right-circle" />
          </Col>
          <Col>
            <Text style={{ marginTop: 5 }}>Em Análise</Text>
          </Col>
        </Row>

        <Row>
          <Col onPress={() => alert('sss')}>
            <Icon style={styles.permissaoVermelha} type="SimpleLineIcons" name="arrow-right-circle" />
          </Col>
          <Col>
            <Text style={{ marginTop: 5 }}>Recusado</Text>
          </Col>
        </Row>
      </Grid>
      {Platform.OS !== 'android' && (
        <View>
          <View style={[styles.divider, {marginTop: 10}]}></View>
            <View style={{flexDirection:"row-reverse",margin:10}}>
              <TouchableOpacity style={{...styles.actions,backgroundColor:"#21ba45"}} 
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.actionText}>OK</Text>
              </TouchableOpacity>  
          </View>
        </View>
      )}
    </View>
  )

  const modalFooter=(
    <View style={styles.modalFooter}>
      <View style={styles.divider}></View>
      <View style={{flexDirection:"row-reverse",margin:10}}>
          <TouchableOpacity style={{...styles.actions,backgroundColor:"transparent"}} 
            onPress={() => {
              setModalVisible(!modalVisible);
            }}>
            <Text style={styles.actionText}></Text>
          </TouchableOpacity>
        <TouchableOpacity style={{...styles.actions,backgroundColor:"transparent"}}>
          <Text style={styles.actionText}></Text>
        </TouchableOpacity>
      </View>
    </View>
  )

  const modalContainer=(
    <View style={styles.modalContainer}>
      {modalHeader}
      {modalBody}
      {modalFooter}
    </View>
  )

  const modal = (
    <Modal
      transparent={false}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}>
      <View style={styles.modal}>
        <View>
          {modalContainer}
        </View>
      </View>
    </Modal>
)

  return (
    <View>
      
      {modal}

      <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
        }}>
        <Icon style={{ fontSize: 40 }} type="EvilIcons" name="question" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modal:{
    backgroundColor:"#00000099",
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalContainer:{
    backgroundColor:"#f9fafb",
    width:"90%",
    borderRadius:5
  },
  modalHeader:{
    
  },
  title:{
    fontWeight:"bold",
    fontSize:20,
    padding:15,
    color:"#000"
  },
  divider:{
    width:"100%",
    height:1,
    backgroundColor:"lightgray"
  },
  modalBody:{
    backgroundColor:"#fff",
    paddingVertical:20,
    paddingHorizontal:10
  },
  modalFooter:{
  },
  actions:{
    borderRadius:5,
    marginHorizontal:10,
    paddingVertical:10,
    paddingHorizontal:20
  },
  actionText:{
    color:"#fff"
  },
  permissaoVerde: {
    color: 'green',
    fontSize: 25
  },
  permissaoAzul: {
    color: 'blue',
    fontSize: 25,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  permissaoLaranja: {
    color: 'orange',
    fontSize: 25
  },
  permissaoVermelha: {
    color: 'red',
    fontSize: 25
  },
  bodyText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  }
});