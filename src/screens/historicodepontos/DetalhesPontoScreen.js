import React, {useState, useEffect} from 'react'
import { Alert, StyleSheet, TextInput, View, Text, ActivityIndicator } from 'react-native'
import HeaderComponent from '../../components/HeaderComponent'
import { Content, Button, Container, Form, Item, Label, Input, Grid, Col, CheckBox, ListItem, FooterTab, Footer, Icon, Card, CardItem, Body } from 'native-base'
import SubHeaderComponent from '../../components/SubHeaderComponent'
import axios from 'axios'
import UrlHelper from '../../constants/UrlHelper'
import { TextInputMask } from 'react-native-masked-text'

export default class DetalhesPontoScreen extends React.Component {
  
    state = {
        isSelected: false,
        id: '',
        hora_ponto: '',
        justificativa_colaborador: '',
        observacao_colaborador: '',
        justificativa_empresa: '',
        observacao_empresa: '',
    };

    UNSAFE_componentWillMount() {
        this.setState({
            id: this.props.route.params.id,
            hora_ponto: this.props.route.params.hora_ponto,
            justificativa_colaborador: this.props.route.params.justificativa_colaborador,
            observacao_colaborador: this.props.route.params.observacao_colaborador,
            justificativa_empresa: this.props.route.params.justificativa_empresa,
            observacao_empresa: this.props.route.params.observacao_empresa,
            permissao: this.props.route.params.permissao,
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            id: nextProps.route.params.id,
            hora_ponto: nextProps.route.params.hora_ponto,
            justificativa_colaborador: nextProps.route.params.justificativa_colaborador,
            observacao_colaborador: nextProps.route.params.observacao_colaborador,
            justificativa_empresa: nextProps.route.params.justificativa_empresa,
            observacao_empresa: nextProps.route.params.observacao_empresa,
            permissao: nextProps.route.params.permissao,
        })
    }

    solicitarCorrecaoPonto = () => {
        this.setState({ isSelected: true });
        axios.post(UrlHelper.apiUrl(`updatebaterponto`), {
            "id": this.state.id,
            "justificativa_colaborador": this.state.justificativa_colaborador,
            "observacao_colaborador": this.state.observacao_colaborador,
            "hora_ponto": this.state.hora_ponto,
        }).then(data => {
            Alert.alert('Sucesso', 'Sulicitação foi enviada, responderemos o mais breve possível.')
            this.setState({ isSelected: false });
            this.props.navigation.navigate('HistoricodepontosScreen', this.state.isSelected);
        })
    }

    render() {
        return (
            <Container>
                <HeaderComponent />
                <Content>
                    <SubHeaderComponent titulo="CORRIGIR PONTO" icone="pencil"/>
                    <View style = {styles.container}>
                    <TextInputMask
                        type={'datetime'}
                        options={
                            {
                            format: 'HH:mm'
                            }
                        }
                        style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Horário"
                        placeholderTextColor = "#9a73ef"
                        autoCapitalize = "none"
                        value={this.state.hora_ponto}
                        onChangeText={(text) => this.setState({hora_ponto: text})}
                    />
                    
                    <TextInput style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Observação"
                        placeholderTextColor = "#9a73ef"
                        autoCapitalize = "none"
                        value={this.state.observacao_colaborador}
                        onChangeText={(text) => this.setState({observacao_colaborador: text})}
                    />
                    <TextInput style = {styles.input}
                        underlineColorAndroid = "transparent"
                        placeholder = "Justificativa"
                        placeholderTextColor = "#9a73ef"
                        autoCapitalize = "none"
                        value={this.state.justificativa_colaborador}
                        onChangeText={(text) => this.setState({justificativa_colaborador: text})}
                    />
                        {/* <Form>
                        <Item stackedLabel>
                            <Label>Horário</Label>
                            <TextInput 
                                style={styles.textInput} 
                                onChangeText={(text) => this.setState({hora_ponto: text})}
                                value={this.state.hora_ponto} 
                                placeholder='Horário' 
                                maxLength={4}
                            />
                        </Item>

                        <Item stackedLabel>    
                            <Label>Observação</Label>
                            <TextInput 
                                style={styles.textInput} 
                                onChangeText={(text) => this.setState({observacao_colaborador: text})}
                                    value={this.state.observacao_colaborador} placeholder='Observação' 
                            />
                        </Item>

                        <Item stackedLabel>
                            <Label>Justificativa</Label>
                            <TextInput 
                                style={styles.textInput} 
                                onChangeText={(text) => this.setState({justificativa_colaborador: text})}
                                    value={this.state.justificativa_colaborador} placeholder='Justificativa' 
                            />
                        </Item> */}
                            {/*
                            <View style={{ marginTop: 30}}>
                                <Grid>
                                    <Col style={{marginLeft: 10}}>
                                        <Grid>
                                            <Col>
                                                <CheckBox
                                                    title="Press me"
                                                    checked={this.state.isSelected}
                                                    onPress={() => this.setState({isSelected: !this.state.isSelected})}
                                                />
                                            </Col>
                                            <Col>
                                                <Button onPress={() => this.setState({isSelected: !this.state.isSelected})} success style={{width: 90, color: 'white'}}>
                                                    <Text></Text>
                                                    <Text style={{color: 'white', fontSize: 22}}>Entrada</Text>
                                                    <Text></Text>
                                                </Button>
                                            </Col>
                                            <Col></Col>
                                            <Col></Col>
                                        </Grid>
                                    </Col>
                                    <Col>
                                        <Grid>
                                            <Col>
                                                <CheckBox
                                                    title="Press me"
                                                    checked={!this.state.isSelected}
                                                    onPress={() => this.setState({isSelected: !this.state.isSelected})}
                                                />
                                            </Col>
                                            <Col>
                                                <Button onPress={() => this.setState({isSelected: !this.state.isSelected})} danger style={{width: 90, color: 'white'}}>
                                                    <Text></Text>
                                                    <Text style={{ color: 'white', fontSize: 22}}>Saida</Text>
                                                    <Text></Text>
                                                </Button>
                                            </Col>
                                            <Col></Col>
                                            <Col></Col>
                                        </Grid>
                                    </Col>
                                </Grid>
                            </View> 
                            */}

                        {/* </Form>  */}
                    </View>
                            
                    {
                        !this.state.observacao_empresa
                        ? <Text></Text>
                        : <Card style={{ marginTop: 25 }}>
                            <CardItem header bordered style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={{color: 'blue'}}>Observação da Empresa</Text>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    <Text>
                                    {this.state.observacao_empresa}
                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    }

                    {
                        !this.state.observacao_empresa
                        ? <Text></Text>
                        : <Card style={{ marginTop: 25 }}>
                            <CardItem header bordered style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={{color: 'blue'}}>Justificativa da Empresa</Text>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    <Text>
                                        {this.state.justificativa_empresa}
                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    }
                </Content>

                {
                    this.state.isSelected == true
                    ? <ActivityIndicator size="large" color="blue"/>
                    : <Footer>
                        <FooterTab>
                            <Button warning onPress={() => this.props.navigation.navigate('HistoricodepontosScreen')}>
                            {/* <Badge ><Text>51</Text></Badge> */}
                                <Icon type="AntDesign" name="back" style={{ fontSize: 25, color: 'orangered' }}/>
                                <Text style={{ color: 'white'}}>Cancelar</Text>
                            </Button>
                
                            {
                                this.state.permissao == false
                                ? null
                                : <Button primary onPress={ () => this.solicitarCorrecaoPonto()}>
                                    <Icon active name="paper" style={{ color: 'white'}}/>
                                    <Text style={{ color: 'white'}}>Solicitar Correção</Text>
                                </Button>
                            }
                        </FooterTab>
                     </Footer>
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    textInput: {
        // fontFamily: 'Montserrat-Regular',
        borderBottomWidth: 1,
        borderColor: '#CCC',
        fontSize: 12,
        width: '100%',
        borderRadius: 5,
        textAlignVertical: 'top',
        paddingVertical: 15,
        paddingHorizontal: 20,
        color: '#666',
      },

      container: {
        paddingTop: 23
     },
     input: {
        margin: 15,
        height: 40,
        borderColor: '#000',
        borderBottomWidth: 1
     },
     submitButton: {
        backgroundColor: '#7a42f4',
        padding: 10,
        margin: 15,
        height: 40,
     },
     submitButtonText:{
        color: 'white'
     }
});