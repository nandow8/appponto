import React, { Component } from 'react';
import styles from './Style'
import {  View, ActivityIndicator, TouchableOpacity, AsyncStorage, Alert } from 'react-native';
import { Button, Fab, Footer, FooterTab, Container, Content, Icon, Text, Body, CardItem, Card } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import SubHeaderComponent from '../../components/SubHeaderComponent';
import HeaderComponent from '../../components/HeaderComponent';
import ModalComponent from './components/ModalComponent';
import axios from 'axios';
import UrlHelper from '../../constants/UrlHelper';

export default class HistoricodepontosScreen extends Component {
  state = {
    showLoading: true,
    historicos: {},
    active: false,
    controlaVisualizacaoOuEdicao: false
  }

  UNSAFE_componentWillMount() {
    this.buscaHistoricoRegistroMarcacaoPonto()
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.buscaHistoricoRegistroMarcacaoPonto()
  }

  buscaHistoricoRegistroMarcacaoPonto = async () => {
    this.setState({ 
      showLoading: true
    })
    let user = await JSON.parse(await AsyncStorage.getItem('user'))
    await axios.get(UrlHelper.apiUrl(`registro_marcacao_ponto/${user.id}`)).then(res => {
      if (res.data.marcacoes_de_pontos)
        this.setState({ historicos: res.data.marcacoes_de_pontos})

      if (res.data.permissoes.consultas_ponto_select_visualizar_editar == 'visualizarEditar')
        this.setState({ controlaVisualizacaoOuEdicao: true})

      this.setState({ 
        showLoading: false
      })
    })
  }

  navegaParaDetalhes(hist) {
    hist.permissao = this.state.controlaVisualizacaoOuEdicao // se mudare de ideia, remoo o if abaixo
    
    if (hist.permissao) {
      this.props.navigation.navigate('DetalhesPontoScreen', hist)
    } else {
      Alert.alert('Atenção', 'Sem permissão para editar seu ponto.')
    }
  }

  render() {
    return (
      <Container>
        <HeaderComponent />
        <Content>
          <SubHeaderComponent titulo="HISTÓRICO DE PONTOS" icone="pencil"/>
        {
          this.state.showLoading == true
          ? <ActivityIndicator  size="large" color="blue"/>
          : <View style={styles.containerCol}>
            {Object.values(this.state.historicos).map((historico, index) => 
              <Card key={historico[0].id}>
                <CardItem header bordered style={styles.headerCard}>
                  <Text style={styles.headerText}>{ historico[0].dia_semana_string }</Text>
                </CardItem>
                <CardItem>
                  <Body>
                    <Grid>
                      {Object.values(historico).map((hist, i) => 
                        <Col key={hist.id}>
                            {
                              hist.permissao == 0 && (
                                <TouchableOpacity style={styles.pontosBatidos} onPress={() => this.navegaParaDetalhes(hist)}>
                                  <Icon style={styles.permissaoLaranja} type="SimpleLineIcons" name="arrow-right-circle" />
                                  <Text style={[styles.tempo]}>{hist.hora_ponto.padStart(5, '0')}</Text>
                                </TouchableOpacity>
                              )
                            }

                            {
                              hist.permissao == 1 && (
                                <TouchableOpacity style={styles.pontosBatidos} onPress={() => this.navegaParaDetalhes(hist)}>
                                  <Icon style={styles.permissaoAzul} type="SimpleLineIcons" name="arrow-right-circle" />
                                  <Text style={[styles.tempo]}>{hist.hora_ponto.padStart(5, '0')}</Text>
                                </TouchableOpacity>
                              )
                            }

                            {
                              hist.permissao == 2 && (
                                <TouchableOpacity style={styles.pontosBatidos} onPress={() => this.navegaParaDetalhes(hist)}>
                                  <Icon style={styles.permissaoVerde} type="SimpleLineIcons" name="arrow-right-circle" />
                                  <Text style={[styles.tempo]}>{hist.hora_ponto.padStart(5, '0')}</Text>
                                </TouchableOpacity>
                              )
                            }

                            {
                              hist.permissao == 4 && (
                                <TouchableOpacity style={styles.pontosBatidos} onPress={() => this.navegaParaDetalhes(hist)}>
                                  <Icon style={styles.permissaoVermelha} type="SimpleLineIcons" name="arrow-right-circle" />
                                  <Text style={[styles.tempo]}>{hist.hora_ponto.padStart(5, '0')}</Text>
                                </TouchableOpacity>
                              )
                            }
                        </Col>
                      )}
                    </Grid>
                  </Body>
                </CardItem>
              </Card>
            )}
            </View>
        }
        </Content>
        
        {/* <Fab
            active={this.state.active}
            direction="left"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF', marginTop: 220 }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}>
            <Icon name="share" />
            <Button style={{ backgroundColor: 'lightblue' }}  onPress={() => this.buscaHistoricoRegistroMarcacaoPonto()}>
              <Icon type="AntDesign" name="reload1"/>
            </Button>
            <Button style={{ backgroundColor: '#3B5998' }}  onPress={() => alert('fa')}>
              <ModalComponent />
            </Button>
            <Button style={{ backgroundColor: 'orange' }}  onPress={() => this.props.navigation.navigate('HomeScreen')}>
              <Icon type="AntDesign" name="arrowleft" />
            </Button>
          </Fab> */}

          <Footer>
              <FooterTab style={{ backgroundColor: 'lightgray'}}>
                  <Button vertical onPress={() => this.props.navigation.navigate('HomeScreen')}>
                    {/* <Badge ><Text>51</Text></Badge> */}
                    <Icon type="MaterialCommunityIcons" name="home" style={{ fontSize: 25, color: 'blue' }}/>
                    <Text style={{ color: '#000'}}>Home</Text>
                  </Button>

                  <Button  vertical>
                    {/* <Badge ><Text>51</Text></Badge> */}
                    <ModalComponent />
                    
                  </Button>
              </FooterTab>
          </Footer>

      </Container>
    );
  }
}
