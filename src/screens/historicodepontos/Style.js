const React = require('react-native')
import { Platform } from "react-native";

export default {
  activityIndicator: {
    flex: 1,
    marginTop: '40%',
    justifyContent: 'center',
    backgroundColor: 'white'
  },

  headerText: {
    flex: 1,
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white'
  },
  headerCard: {
    backgroundColor: '#5F9EA0'
  },

  pontosBatidos: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    border: 1,
  },

  tempo: {
    marginTop: '5%',
    marginLeft: '2%',
    fontSize: 14
  },

  permissaoVerde: {
    color: 'green',
    fontSize: 25
  },
  permissaoAzul: {
    color: 'blue',
    fontSize: 25
  },
  permissaoLaranja: {
    color: 'orange',
    fontSize: 25
  },
  permissaoVermelha: {
    color: 'red',
    fontSize: 25
  },
  containerCol: {
    paddingLeft: '3%',
    paddingRight: '3%',
  },

  footerStyle: {
    backgroundColor: Platform.OS === 'android' ? 'white' : '',
    paddingLeft: '2%',
    paddingRight: '2%',
  },
  textFooter: {
    color: Platform.OS === 'android' ? 'white' : ''
  },

  footerButton: {
    backgroundColor: Platform.OS === 'android' ? 'white' : ''
    //borderBottomLeftRadius: 30,
    //borderBottomRightRadius: 30,
    //borderTopLeftRadius: 30,
    //borderTopRightRadius: 30,
  }
} 