import React, { Component } from 'react';
import { Alert, Text, TextInput, View, Image, TouchableOpacity, ScrollView, ActivityIndicator } from "react-native";
import { Item, Input, Icon } from 'native-base';
import styles from './Style'
import axios from 'axios';
import UrlHelper from '../../constants/UrlHelper';
import { AsyncStorage } from 'react-native';

export default class LoginScreen extends Component {
    logar = () => {
        this.setState({ isLoading: true });

        axios.post(UrlHelper.apiUrl('login'), {codigo: this.state.codigo.toUpperCase(), email: this.state.email, password: this.state.password}).then(res => {
            //res.data.user.acesso_ao_sistema == 0 ||
            if (res.data.user_colaboradores.permite_ponto_aplicativo == 0 || res.data.user_colaboradores.acesso_ao_sistema == 0) {
                Alert.alert('Atenção', 'Seu acesso está inabilitado no sistema, por favor contate o RH da empresa.')
                this.setState({ isLoading: false });
            } else {
                AsyncStorage.setItem('token', res.data.token);
                AsyncStorage.setItem('user', JSON.stringify(res.data.user));
                AsyncStorage.setItem('user_colaboradores', JSON.stringify(res.data.user_colaboradores));

                this.props.navigation.navigate('HomeScreen');
                this.setState({ isLoading: false });
            }
        }).catch(err => {
            Alert.alert('Atenção', 'Dados inválidos, tente novamente')
            this.setState({ isLoading: false });
        })
    }

    state = {
        codigo: '',
        email: '',
        password: '',
        isLoading: false
    }

    render() {
        return (
        <ScrollView style={styles.container}>
            <View>
                <View style={{ marginTop: '20%', alignItems: "center", justifyContent: "center" }}>
                    <Image source={require("../../../assets/images/avatarpequeno.png")} />
                </View>
                <View style={{ marginBottom: 45, alignItems: "center", justifyContent: "center" }}>
                    <Text style={[styles.text, { fontSize: 39, fontWeight: "500", color: '#999' }]}>PONTO DIGITAL</Text>
                </View>
                 {/* <View style={{ marginTop: 48, flexDirection: "row", justifyContent: "center" }}>
                        <TouchableOpacity>
                            <View style={styles.socialButton}>
                                <Image source={require("../../../assets/images/facebook.png")} style={styles.socialLogo} />
                                <Text style={styles.text}>Facebook</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.socialButton}>
                            <Image source={require("../../../assets/images/google.png")} style={styles.socialLogo} />
                            <Text style={styles.text}>Google</Text>
                        </TouchableOpacity>
                    </View>  

                    <Text style={[styles.text, { color: "#ABB4BD", fontSize: 15, textAlign: "center", marginVertical: 20 }]}>or</Text>
                */}

                <View style={styles.inputTitle}>
                    <Item>
                        <Icon active type="FontAwesome" name="lock"  style={{ fontSize: 25, color: '#38d39f' }}/>
                        <Input 
                            maxLength={4}
                            placeholder='Código da Empresa'
                            onChangeText={(text) => this.setState({ codigo: text.toUpperCase()})}
                        />
                    </Item>
                </View>

                <View style={styles.inputTitle}>
                    <Item>
                        <Icon active type="FontAwesome" name="user"  style={{ fontSize: 25, color: '#38d39f' }}/>
                        <Input 
                            placeholder='Email ou Matrícula'
                            onChangeText={(text) => this.setState({ email: text})}
                        />
                    </Item>
                </View>

                <View style={styles.inputTitle}>
                    <Item>
                        <Icon active type="FontAwesome" name="lock" style={{ fontSize: 25, color: '#38d39f' }}/>
                        <Input 
                            placeholder='Senha'
                            onChangeText={(text) => this.setState({ password: text})}
                            secureTextEntry={true}
                        />
                    </Item>
                </View>

                {/* <Text style={[styles.text, styles.link, { textAlign: "right" }]}>Esqueceu sua senha?</Text> */}

                <ActivityIndicator animating={this.state.isLoading} />
                <TouchableOpacity style={styles.submitContainer} onPress={() => this.logar()}>
                    <Text style={[ styles.text, { color: "#FFF", fontWeight: "600", fontSize: 22}]}>
                        LOGIN
                    </Text>
                </TouchableOpacity>

                <Text
                    style={[
                        styles.text,
                        {
                            fontSize: 14,
                            color: "#ABB4BD",
                            textAlign: "center",
                            marginTop: 24
                        }
                    ]}
                >
                    {/* Don't have an account? <Text style={[styles.text, styles.link]}>Register Now</Text> */}
                </Text>
            </View>
        </ScrollView>
        );
    }
}