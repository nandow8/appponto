import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    View,
    Alert,
    ImageBackground,
    Image,
} from 'react-native';
import axios from 'axios'
import UrlHelper from '../../constants/UrlHelper';


export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {
        const user = await AsyncStorage.getItem('user');        
        const {id} = JSON.parse(user);
        
        axios.get(UrlHelper.apiUrl(`verificapermissoes/${id}`)).then(res => {
            AsyncStorage.removeItem('user_colaboradores');
            AsyncStorage.setItem('user_colaboradores', JSON.stringify(res.data.user_colaboradores));

            this.props.navigation.navigate('HomeScreen');
        }).catch(err => {
            AsyncStorage.removeItem('token');
            AsyncStorage.removeItem('user');
            AsyncStorage.removeItem('user_colaboradores');
            
            this.props.navigation.navigate('LoginScreen');
            Alert.alert('Atenção', 'Seu acesso está inabilitado no sistema, por favor contate o RH da empresa.')
        })
        
    };

    // Render any loading content that you like here 
    //<ActivityIndicator size="large" color="blue"/>
                //<StatusBar barStyle="default" />
    render() {
        return (
                <ImageBackground source={require('../../../assets/splash.png')} style={{ flex:1, alignItems: 'center'}} >
                    
                </ImageBackground>
        );
    }
}