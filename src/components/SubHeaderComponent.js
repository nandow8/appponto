import React, { Component } from 'react';
import {  View, StyleSheet, Text } from 'react-native';
import { Icon} from 'native-base';

export default class SubHeaderComponent extends Component {
  
  render() {
    return (
        <View full style={styles.buttonHeader}>
            <Icon type="Entypo" name={this.props.icone} style={styles.icone} />
            <Text style={styles.textButton}>{this.props.titulo}</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    buttonHeader: {
        backgroundColor: '#F5F5F5',
        paddingBottom: 1,
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
    },

    textButton: {
        textAlign: 'center',
        color: '#000',
        fontSize: 14,
        padding: 3
    },

    icone: {
        marginTop: 2,
        fontSize: 16
    }

})