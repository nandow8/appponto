import React, { Component } from 'react';
import {  View, StyleSheet, Text, Image, AsyncStorage } from 'react-native';
import UrlHelper from '../constants/UrlHelper';

export default class HeaderComponent extends Component {
  state = {
    photo: ''
  }
  userCredentials = async () => {
    let {photo} = await JSON.parse(await AsyncStorage.getItem('user'))

    this.setState({photo})
  }

  componentDidMount() {
    this.userCredentials()
  }

  render() {
    return (
        <View style={styles.container}>
          <View style={styles.overlay}>
            <Text style={styles.headline}></Text>
            {
              this.state.photo == null && this.state.photo == undefined && this.state.photo == ''
              ? <Image style={styles.logo} source={require('../../assets/images/avatarpequeno.png')} />
              : <Image style={styles.logo} source={{ uri: UrlHelper.apiImageUrl(this.state.photo)}} />
            }
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1, 993622
        alignItems: 'center',
        backgroundColor: 'gray'
    },
        overlay: {
        opacity: 0.5,
    },
    logo: {
        width: 160,
        height: 100,
        marginBottom: 10
    },
        headline: {
        fontSize: 18,
        textAlign: 'center',
        color: 'white'
    },

})