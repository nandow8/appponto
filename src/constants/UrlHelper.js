import Constants from 'expo-constants';

function getMode(){
    try {
        let channel = Constants.manifest.releaseChannel;
        if(channel === 'develop' || channel === undefined){
            return 1;
        }

        return 0;
    }
    catch{
        return 1;
    }
}

const UrlHelper = {
    developerMode: getMode(),
    apiUrl: (url) => {
        if(UrlHelper.developerMode) {
            return 'http://192.168.1.8:8000/api/'+url;
        }
        else{
            return 'https://www.app.pontodigitalapp.com.br/api/'+url;
        }
    },
    apiImageUrl: (imagemPath) => {
        if(UrlHelper.developerMode) {
            return 'http://192.168.1.8:8000/'+imagemPath;
        }
        else{
            return 'https://www.app.pontodigitalapp.com.br/public/'+imagemPath;
        }
    },
    
}

export default UrlHelper;