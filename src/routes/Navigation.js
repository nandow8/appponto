import React, {useState, useEffect} from 'react';
import { View, Text, ImageBackground, Image, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native';
import { createDrawerNavigator,  DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import Menu from './menu/Menu'
import {Icon} from 'native-base'
import LoginScreen from '../screens/auth/LoginScreen'
import HomeScreen from '../screens/home/HomeScreen'
import AuthLoadingScreen from '../screens/auth/AuthLoadingScreen';
import HistoricodepontosScreen from '../screens/historicodepontos/HistoricodepontosScreen'
import CameraBaterPonto from '../screens/home/CameraBaterPonto';
import DetalhesPontoScreen from '../screens/historicodepontos/DetalhesPontoScreen';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import Styles from '../routes/menu/Style'
import VerificaPermissoesUsuario from '../screens/auth/VerificaPermissoesUsuario';

const Drawer = createDrawerNavigator();

function DrawerContent(props) {
  const [user, setUser] = useState(0);
  
  useEffect(() => {
    const getUser = async () => {
      try {
        const user = await AsyncStorage.getItem('user');
        const value = JSON.parse(user);
        // Other set states
        setUser(value);
      } catch (error) {
        console.log(error);
      }
    };
    getUser()
  }, [])

  return (
    <DrawerContentScrollView {...props}>
      <View style={Styles.nav}>
        <View style={Styles.navProfile}>
        <ImageBackground source={require('../../assets/images/bg.png')} style={Styles.navImg} >
          <Image style={Styles.navAvatar} source={require('../../assets/images/avatar.png')} />
            <Text style={Styles.navName}>{user ? user.name : ''}</Text>
          </ImageBackground>
          
        </View>
        <View style={Styles.navMenu}>
          <ScrollView>
            <TouchableOpacity style={Styles.profileItem} underlayColor='transparent' onPress={() => {
              props.navigation.dispatch(DrawerActions.toggleDrawer())
              props.navigation.navigate('HomeScreen')
            }}>
              <View style={Styles.navBtn}>
                <View style={Styles.navBtnLeft}>
                  <Icon name="home" type="FontAwesome" style={Styles.navBtnIcon} />
                </View>
                <Text style={Styles.navBtnText}>Home</Text>
              </View>
            </TouchableOpacity>
            
            <TouchableOpacity style={Styles.profileItem} underlayColor='transparent' onPress={() => {
              props.navigation.dispatch(DrawerActions.toggleDrawer())
              props.navigation.navigate('HistoricodepontosScreen')
            }}>
              <View style={Styles.navBtn}>
                <View style={Styles.navBtnLeft}>
                  <Icon name="building-o" type="FontAwesome" style={Styles.navBtnIcon} />
                </View>
                <Text style={Styles.navBtnText}>Histórico</Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
      {/* <DrawerItem
        label="Close drawer"
        onPress={() => props.navigation.navigate('HomeScreen')}
      />
      <DrawerItem
        label="Toggle drawer"
        onPress={() => props.navigation.dispatch(DrawerActions.toggleDrawer())}
      /> */}
    </DrawerContentScrollView>
  );
}

const RootNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props}/>}>
      <Drawer.Screen name="AuthLoadingScreen" component={AuthLoadingScreen} />
      <Drawer.Screen name="LoginScreen" component={LoginScreen} />
      <Drawer.Screen name="VerificaPermissoesUsuario" component={VerificaPermissoesUsuario} />

      <Drawer.Screen name="HomeScreen" component={HomeScreen} />
      <Drawer.Screen name="CameraBaterPonto" component={CameraBaterPonto} />

      <Drawer.Screen name="HistoricodepontosScreen" component={HistoricodepontosScreen} />
      <Drawer.Screen name="DetalhesPontoScreen" component={DetalhesPontoScreen} />
      
    </Drawer.Navigator>
  );
};

export default RootNavigator