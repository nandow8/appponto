import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native';
import {Icon} from 'native-base'
import Styles from './Style'
import * as MENU from './MenuList'

export default class Menu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      user: []
    }
    this.renderMenuList = this.renderMenuList.bind(this)
  }

  UNSAFE_componentWillMount() {
      this.loadAsyncStorage()
  }

  async loadAsyncStorage() {
    let users = await JSON.parse(await AsyncStorage.getItem('user'))
    this.setState({
      user: users
    })

    // console.log(this.state.user);
  }

  renderMenuList(menus) {
    return menus.map((menu, i) => {
      return <TouchableOpacity key={i} style={Styles.profileItem} underlayColor='transparent' onPress={() => {
        NavigationService.closeDrawer()
        NavigationService.navigate(menu.route)
      }}>
        <View style={Styles.navBtn}>
          <View style={Styles.navBtnLeft}>
            <Icon name={menu.iconName} type={menu.iconType} style={Styles.navBtnIcon} />
          </View>
          <Text style={Styles.navBtnText}>{menu.name}</Text>
        </View>
      </TouchableOpacity>
    })
  }

  render() {
    return (
      <View style={Styles.nav}>
      <View style={Styles.navProfile}>
      <ImageBackground source={require('../../../assets/images/bg.png')} style={Styles.navImg} >
        <Image style={Styles.navAvatar} source={require('../../../assets/images/avatar.png')} />
          <Text style={Styles.navName}>{this.state.user.name}</Text>
        </ImageBackground>
        
      </View>
      <View style={Styles.navMenu}>
        <ScrollView>
          {this.renderMenuList(MENU.Data1)}
          {/* <Text style={Styles.navHeader}>Member</Text>
          {this.renderMenuList(MENU.Data2)} */}
        </ScrollView>
      </View>
    </View>
    );
  }
}