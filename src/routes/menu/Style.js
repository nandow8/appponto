const React = require('react-native')
const { Platform, Dimensions } = React

const deviceHeight = Dimensions.get('window').height
const deviceWidth = Dimensions.get('window').width

export default {

  layout: {
    flex: 1,
    width: '100%'
  },
  navImg: {
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  nav: {
    flex: 1,
    width: '100%'
  },
  navProfile: {
    width: '100%',
  },
  navAvatar: {
    marginTop: 15,
    width: 64,
    height: 64,
    borderRadius: 37
  },
  navName: {
    fontSize: 14,
    //fontFamily: 'Roboto',
    color: '#FFF',
    marginTop: 10
  },
  navMenu: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255,1)',
    paddingTop: 15,
    paddingBottom: 15,
    paddingHorizontal: 5
   
  },
  navHeader: {
    //fontFamily: 'Roboto',
    fontSize: 14,
    color: '#333',
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20
  },
  profileItem: {
    marginTop: 10,
    marginBottom: 10
  },
  navBtn: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  navBtnLeft: {
    width: 30,
    alignItems: 'center',
    marginRight: 20
  },
  navBtnIcon: {
    fontSize: 24,
    color: '#000'
  },

  navBtnText: {
    //fontFamily: 'Roboto',
    fontSize: 12,
    color: 'rgba(0,0,0,0.8)'
  },
  profileMenuIcon: {
    color: '#333'
  },
}