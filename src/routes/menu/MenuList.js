export const Data1 = [

    {
      name: 'Home',
      route: 'HomeScreen',
      iconName: 'home',
      iconType: 'FontAwesome'
    },
    {
      name: 'Histórico de Pontos',
      route: 'HistoricodepontosScreen',
      iconName: 'building-o',
      iconType: 'FontAwesome'
    },
    
  ]


  // export const Data1Backup = [

  //   {
  //     name: 'Home',
  //     route: 'PublicHome',
  //     iconName: 'home',
  //     iconType: 'FontAwesome'
  //   },
  //   {
  //     name: 'Properties',
  //     route: 'PublicProperties',
  //     iconName: 'building',
  //     iconType: 'FontAwesome'
  //   },
  //   {
  //     name: 'Property Detail',
  //     route: 'PublicPropertyDetail',
  //     iconName: 'building-o',
  //     iconType: 'FontAwesome'
  //   },
  //   {
  //     name: 'Property Search',
  //     route: 'PublicPropertySearch',
  //     iconName: 'search',
  //     iconType: 'FontAwesome'
  //   },
  //   {
  //     name: 'Agents',
  //     route: 'PublicAgents',
  //     iconName: 'group',
  //     iconType: 'FontAwesome'
  //   },
  //   {
  //     name: 'Agent Detail',
  //     route: 'PublicAgentDetail',
  //     iconName: 'user-circle-o',
  //     iconType: 'FontAwesome'
  //   },
  //   {
  //     name: 'About Myyaow Realtor',
  //     route: 'PublicAboutUs',
  //     iconName: 'info-circle',
  //     iconType: 'FontAwesome'
  //   },
  //   {
  //     name: 'Contact',
  //     route: 'PublicContact',
  //     iconName: 'envelope',
  //     iconType: 'FontAwesome'
  //   }
  // ]
  
  export const Data2 = [
    {
      name: 'Register',
      route: 'MemberSignUp',
      iconName: 'lock',
      iconType: 'FontAwesome'
    },
    {
      name: 'Sign In',
      route: 'MemberSignIn',
      iconName: 'login-variant',
      iconType: 'MaterialCommunityIcons'
    },
    {
      name: 'Dashboard',
      route: 'MemberHome',
      iconName: 'home',
      iconType: 'FontAwesome'
    },
    {
      name: 'Properties',
      route: 'MemberProperties',
      iconName: 'building',
      iconType: 'FontAwesome'
    },
    {
      name: 'Add Property',
      route: 'MemberPropertyAdd',
      iconName: 'plus',
      iconType: 'FontAwesome'
    },
    {
      name: 'Messages',
      route: 'MemberMessages',
      iconName: 'message',
      iconType: 'MaterialCommunityIcons'
    },
    {
      name: 'Profile',
      route: 'MemberProfile',
      iconName: 'user-circle-o',
      iconType: 'FontAwesome'
    },
    {
      name: 'Favorites',
      route: 'MemberFavorites',
      iconName: 'star',
      iconType: 'FontAwesome'
    },
    {
      name: 'Settings',
      route: 'MemberSettings',
      iconName: 'gears',
      iconType: 'FontAwesome'
    },
    {
      name: 'Logout',
      route: 'PublicHome',
      iconName: 'sign-out',
      iconType: 'FontAwesome'
    }
  ]